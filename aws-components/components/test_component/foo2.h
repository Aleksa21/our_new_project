#pragma once
#ifdef __cplusplus
	extern "C" {
#endif

int surface(int a, int b);

int double_surface(int s);

#ifdef __cplusplus
}
#endif
