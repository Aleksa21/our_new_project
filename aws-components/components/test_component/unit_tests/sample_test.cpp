#include <catch2/catch_all.hpp>
//#include "../foo.h"
#include "../foo2.h"
#include "fff.h"

DEFINE_FFF_GLOBALS;
FAKE_VOID_FUNC(DISPLAY_output);
FAKE_VALUE_FUNC(int, double_value);

TEST_CASE("Our first test", "[test]")
{
	int a = 5;

	double_value_fake.return_val = 2 * a;
	int a_double = double_value_fake.return_val;
	int b = 10;
	int s = surface(a, b);
	int s_double = double_surface(s);
	
	double_value_fake.return_val = 3 * a;
	int a2_double = double_value();
	
	REQUIRE(double_value_fake.call_count == 1);
	REQUIRE(s == 50);
	REQUIRE(a_double == 10);
	REQUIRE(s_double == 100);
	REQUIRE(a2_double == 10);
}