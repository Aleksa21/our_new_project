cmake_minimum_required(VERSION 3.5)
#cmake_minimum_required is necessary at the top of the top level cmake file

#give project the name, tell it that the language is c++
project(core_mqtt_tests LANGUAGES C CXX)

set(COMPONENTS_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../../../components")

#tell cmake where Catch2 library is => how to build it
add_subdirectory(lib/Catch2)
add_subdirectory(${COMPONENTS_DIR}/test_component/unit_tests whatever)
#add_subdirectory(${COMPONENTS_DIR}/esp-cli/unit_tests esp_cli)

#Add an executable 
add_executable(unit_tests)

#link the executable with it's libraries
target_link_libraries(unit_tests
	Catch2::Catch2WithMain
	sample_tests	
)

add_executable(unit_tests_2)

target_link_libraries(unit_tests_2
	Catch2::Catch2WithMain
	sample_tests_2	
)

#add_executable(esp_cli_unit_test)
#
#target_link_libraries(esp_cli_unit_test
#	Catch2::Catch2WithMain
#	esp_cli_tests	
#)